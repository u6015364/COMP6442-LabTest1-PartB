package zzh.a6442labtest1;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class ScribbleView extends View implements View.OnTouchListener{

    ArrayList<Point> circles = new ArrayList<Point>();
    float r = 10.0f;

    private class Point{
        float x;
        float y;

        Point(float x, float y){
            this.x = x;
            this.y = y;
        }
    }

    public ScribbleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setOnTouchListener(this);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        p.setColor(Color.BLUE);

        for(Point c : circles){
            canvas.drawCircle(c.x, c.y, r, p);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            circles.add(new Point(event.getX(), event.getY()));
            this.invalidate();

        }
        else if(event.getAction() == MotionEvent.ACTION_MOVE){

            circles.add(new Point(event.getX(), event.getY()));
            this.invalidate();

        }
        return true;
    }
}
