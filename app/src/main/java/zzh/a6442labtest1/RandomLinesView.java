package zzh.a6442labtest1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.Random;

public class RandomLinesView extends View{

    public RandomLinesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        p.setColor(Color.BLUE);
        p.setStrokeWidth(10.0f);

        float width = canvas.getWidth();
        float height = canvas.getHeight();

        Random ran =new Random();

        for(int i = 0; i<50; i++){

            float x1 = ran.nextFloat()*width;
            float x2 = ran.nextFloat()*width;
            float y1 = ran.nextFloat()*height;
            float y2 = ran.nextFloat()*height;
            canvas.drawLine(x1, y1, x2, y2,p);
        }
    }
}

