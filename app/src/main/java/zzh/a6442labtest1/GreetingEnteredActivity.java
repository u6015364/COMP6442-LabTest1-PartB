package zzh.a6442labtest1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class GreetingEnteredActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting_entered);

        Intent intent = getIntent();
        String info = intent.getStringExtra("KEY");
        TextView t = (TextView) findViewById(R.id.textView2);
        t.setText("Hello " + info + "!!!");
    }
}
