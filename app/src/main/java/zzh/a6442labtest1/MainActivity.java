package zzh.a6442labtest1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void toScribble(View view) {
        Intent intent = new Intent(this, ScribbleActivity.class);
        startActivity(intent);
    }

    public void toCountdown(View view) {
        Intent intent = new Intent(this, CountdownActivity.class);
        startActivity(intent);
    }

    public void toMenus(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    public void toRandomlines(View view) {
        Intent intent = new Intent(this, RandomLinesActivity.class);
        startActivity(intent);
    }

    public void toGrettingApp(View view) {
        Intent intent = new Intent(this, GreetingActivity.class);
        startActivity(intent);
    }
}
