package zzh.a6442labtest1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class GreetingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);
    }

    public void onEnter(View view) {
        Intent intent = new Intent(this, GreetingEnteredActivity.class);
        EditText et = (EditText) findViewById(R.id.editText);
        
        intent.putExtra("KEY",et.getText().toString());
        startActivity(intent);
    }
}
