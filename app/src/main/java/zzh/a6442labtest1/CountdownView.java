package zzh.a6442labtest1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class CountdownView extends View implements View.OnTouchListener, Runnable {

    Handler timer;
    static int initCount = 10;
    int count = initCount;
    float tx = 150.0f;
    float ty = 150.0f;

    public CountdownView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setOnTouchListener(this);

        timer = new Handler();
        timer.postDelayed(this,1000);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        p.setColor(Color.BLUE);
        p.setTextSize(80);
        canvas.drawText(String.valueOf(count),tx, ty, p);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            count  = initCount;
            this.invalidate();
        }
        return true;
    }

    @Override
    public void run() {

        count --;
        this.invalidate();
        timer.postDelayed(this,1000);

    }
}
